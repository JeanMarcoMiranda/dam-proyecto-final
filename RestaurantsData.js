const RestaurantsData = [
  {
      id: 1,
      name: "Sambambaias",
      shortDescription: "Hemos creado un ambiente natural, elegante y acogedor, que te sumergirá sutilmente en una atmósfera de confort.",
      longDescription: "Permítenos cautivarte con nuestra exquisita comida, nuestra esmerada atención y nuestros elegantes y refrescantes ambientes, diseñados para deslumbrar a nuestros comensales a través de una decoración que fusiona el estilo arquitectónico de las tradicionales casas arequipeñas con marcados detalles modernos y mucha presencia natural que logrará sumergirte en un disfrute pleno entre belleza, arte y sabor.",
      urlImage: "https://content.emarket.pe/uploads/sites/22/2016/01/1400000015081.jpg",
      location: {
          lat: -16.4055057,
          lon: -71.5421357,
      }
  },
  {
      id: 2,
      name: "Passion Cocina Peruana",
      shortDescription: "“Passion” ofrece sus increíbles desayunos buffet por la mañana, al igual almuerzos y cenas para que puedas saborear durante todo el día de nuestra carta.",
      longDescription: "“Passiones” nos ofrece sus increíbles desayunos buffet con una gran variedad de cereales, frutas de estación, lácteos, panes, jugos frescos y naturales; Almuerzos y cenas para que puedas saborear durante todo el día de nuestra carta con comida elaborada con los mejores ingredientes de nuestra región resaltando los sabores típicos de nuestra cocina. Estamos estableciendo protocolos de seguridad y creando nuevas propuestas para que ustedes puedan disfrutar de nuestros platos sin salir de casa.",
      urlImage: "https://media-cdn.tripadvisor.com/media/photo-s/19/cd/37/d1/cena-romantica.jpg",
      location: {
          lat: -16.4035541,
          lon: -71.5414543,
      }
  },
  {
      id: 3,
      name: "AQP Burger Company",
      shortDescription: "Brindamos: - Variedad en sanguches, especializandonos en hamburguesas artesanales. - Variedad en salchipapas. - Cualquier pedido, nuestro servicio brinda 10 tipos de cremas. *Nos basamos en la calidad, higiene y sobre todo un buen ambiente para compartir",
      longDescription: "Hoy recordamos y conmemoramos la determinación de los soldados que no se rindieron y pelearon en defensa de sus ideales y del país. Hoy recordamos el Día de la bandera en conmemoración a un acto de heroísmo que marcaría la historia en el Perú. La promesa de AQP Burger Company es darte una propuesta fresca y dinámica. Entonces, que esperas ☺️para probar nuestra combinación perfecta!!. Hasta ayer culminó la promoción de la segunda hamburguesa a mitad de precio 🤠. Gracias a cada una de las personas que realizaron sus pedidos, y mas aun cuando devolvieron mensajes motivadores y alientadores.",
      urlImage: "https://media-cdn.tripadvisor.com/media/photo-s/19/38/1c/de/20190912-142726-largejpg.jpg",
      location: {
          lat: -16.4005039,
          lon: -71.5393555,
      }
  },
  {
      id: 4,
      name: "Renato's Grill",
      shortDescription: "Restaurante inspirado en la mezcla y toques de sabores, texturas y colores ubicados en el hall de entrada al Hotel y Centro de Eventos Malibu en la ciudad de Sincelejo. Animado con una brisa fresca en el centro de una arquitectura moderna, con una gran guaca de sabores.",
      longDescription: "restaurante en Sincelejo inspirado en la mezcla y toques de sabores, texturas y colores ubicados en el hall de entrada del hotel malibu ubicado en la ciudad de Sincelejo. Animado con una brisa fresca en el centro de una arquitectura moderna, con una gran guaca de sabores. Nuestro menú es una experiencia ecléctica de la mejor cocina internacional con un toque de estilo mediterráneo. De sabrosas pastas hechas en casa, hasta exquisitos sabores de carne, pescados, mariscos y sabores tradicionales, servidas plato por plato con toques especiales de decoración y alta cocina. Sirviendo a la mesa y en buffet, desayuno, almuerzo y comida en todos los horarios disponibles de 6:00 a 10:00 am de 1200-3:00 Pm y de 6:00-11: 00 pm de Lunes a Sábado.La Guaquería es un restaurante casual, enérgico y cotidiano. Conocido por el estilo mediterráneo y con espacios al aire libre.",
      urlImage: "https://media-cdn.tripadvisor.com/media/photo-s/12/a7/1b/1f/una-experiencia-que-se.jpg",
      location: {
          lat: -16.407814,
          lon: -71.5542953,
      }
  },
  {
      id: 5,
      name: "Vikingos Arequipa",
      shortDescription: "Vikingos Medieval Grill, te invita a degustar de sus exquisitas parrillas familiares. No te las pierdas están increíbles, pídelas a través de los siguientes números 925635330 o 951135812.",
      longDescription: "El comedor Da Paloma está pensado para albergar pequeñas celebraciones o comidas de empresa, con una capacidad de hasta ochenta personas. Además dispone de proyector y pantalla lo que posibilita su utilización para presentaciones y eventos similares.El local, inaugurado en el verano de 2004, ofrece cocina tradicional con toques de modernidad. El restaurante aprovecha los productos de la zona ( excelentes carnes, salazones, pulpo, productos de la huerta, pescados de la cercana ría de Vigo…) combinándolos con nuevos sabores y texturas.",
      urlImage: "https://i.ytimg.com/vi/L2swM9w0TqY/maxresdefault.jpg",
      location: {
          lat: -16.4013158,
          lon: -71.550644,
      }
  },
  {
      id: 6,
      name: "El Submarino",
      shortDescription: "Y tu que esperas para probarnos!!! Todas las delicias marinas a un precio Justo y con un sabor increíble que te darán ganas de repetirlo!!!!! Visitanos también en la Av. San Jerónimo 504 umacollo al frente de la iglesia los capuchinos o al frente de la U. Católica",
      longDescription: "La Cevichería El Submarino partió como un pequeño negocio familiar de venta de ceviches en el garaje de una casa. Poco a poco la clientela fue creciendo, pues la gente que trabajaba cerca fue pasando de boca en boca el secreto de sus deliciosos ceviches, y así un día no les quedó más que instalar un restaurant que albergara a todos sus fanáticos. Hoy la Cevichería El Limón sigue deleitando a sus comensales con deliciosos ceviches y además ofrece jaleas, tiraditos, tacu-tacu y todo tipo de maravillas peruanas preparadas con cariño y dedicación.",
      urlImage: "https://media-cdn.tripadvisor.com/media/photo-s/14/8a/45/65/iel-mirador-te-espera.jpg",
      location: {
          lat: -16.4067526,
          lon: -71.5478699,
      }
  },
  {
      id: 7,
      name: "La Calvet",
      shortDescription: "La Calvet, inicia actividades en mayo del 2018, ofreciendo a nuestra selecta clientela lo mejor de la comida criolla e internacional, con recetas muy caseras, manteniendo la tradición y sabores de antaño. Atendemos toda clase de eventos sociales.",
      longDescription: "La Calvet, ofrece a su clientela lo mejor de la comida criolla e internacional, con recetas muy caseras, manteniendo la tradición y sabores de antaño. Atiende toda clase de eventos sociales en su local de calle Ricardo Palma 103 Umacollo, Arequipa. Gracias por su colaboración en #Lidera2019 La Calvet.Restaurant La Calvet acatando las medidas de protección, seguridad y salubridad dispuestas por el Gobierno; pone a vuestra disposición el servicio delivery. Para toda nuestra clientela. Haga sus reservas con anticipación, para estar previstos con el reparto. Todos somos responsables de nuestra seguridad. Los peruanos unidos sí podemos! Gracias por la atención y preferencia.",
      urlImage: "https://estaticos.elperiodico.com/resources/jpg/6/9/zentauroepp47876676-barcelona-2019-barcelona-barceloneando-restaurant190426142827-1556281902996.jpg",
      location: {
          lat: -16.4006643,
          lon: -71.5467576,
      }
  },
  {
      id: 8,
      name: "Banana's Crunch",
      shortDescription: "Les contamos que a partir del día de mañana, LUNES 1 DE JUNIO, los atenderemos con el cariño de siempre. Ahora, podrán hacer sus pedidos a través de sus dispositivos móviles en nuestra TIENDA ONLINE. Los invitamos a que vean el siguiente video  para que en 6 pasos SÚPER SENCILLOS disfruten de unos productos deliciosos",
      longDescription: "Somos una empresa artesanal productora y comercializadora de alimentos de excelente calidad, comprometida en deleitar con cariño y calidez a la comunidad en que nos desenvolvemos. Dedicamos el tiempo necesario a las necesidades de cada cliente. En Dulces y Postres de Arilog  creamos deliciosos dulces y mini repostería , con exquisitas recetas, sabores de toda la vida o combinaciones originales. Hechos con ingredientes de la más alta calidad, con la más fina elaboración y precios accesibles. Así conseguimos que nuestros productos sean una delicia para los ojos y un capricho para el paladar, que se puede disfrutar en cualquier momento. Ofrecemos deliciosos mini postres, galletas, alfajores, tortas y muffins para cualquier evento, social.   Estamos horneando para ti,!",
      urlImage: "https://r-cf.bstatic.com/images/hotel/max1024x768/177/177657398.jpg",
      location: {
          lat: -16.4046245,
          lon: -71.5477197,
      }
  },
  {
      id: 9,
      name: "Gran Carbón",
      shortDescription: "Porque nos preocupamos por tu salud y de nuestros trabajadores, Gran Carbón cuenta con autorización del Minsa para operar. Disculpa cualquier demora pero lo estamos haciendo con mas seguridad para el disfrute de tu pedido.",
      longDescription: "¡Salchipapas tienen muchos!.. pero VERDADERAS SALCHIBRASAS PREMIUM con embutidos de CALIDAD ALEMANA, ¡SOLO EN GRAN CARBÓN! Utilizamos embutidos de Primera Calidad: Embutidos Braed como partner (Calidad Alemana). El corte de nuestra Salchicha Frankfurter es de corte alemán (grueso), para que puedas sentir el verdadero sabor de un Frankfurter. Utilizamos papa nacional de Calidad y no importada. Contamos con la autorización de los ministerios de la Producción y de Salud (N° 009437-2020). Solicita nuestra nueva carta digital por WhatsApp. Horario de atención: 12:00 p.m. a 7:00 p.m. (Lunes a Sábado por ahora)",
      urlImage: "https://lh5.googleusercontent.com/p/AF1QipM33lgu0_NkEWaDCR9IJQ3rSPxy6fzgsKQSW81u=w408-h272-k-no",
      location: {
          lat: -16.4005111,
          lon: -71.550461,
      }
  },
  {
      id: 10,
      name: "Patatas",
      shortDescription: "Pronto tendrás el #POLLITO a la BRASA que tanto extrañas en tu mesa.Cumpliremos los protocolos de bioseguridad necesarios para garantizar la salud de tu familia y la de nuestros colaboradores. #Delivery #polloalabrasa #patatas #ofertas #quedateencasa #pronto",
      longDescription: "Se trata de una preparación culinaria sencilla que se realiza en dos etapas: la primera es una fritura y la segunda un guiso. El principal ingrediente son las patatas que se suelen cortar en rodajas de un grosor aproximado de un centímetro. Suelen rebozarse primero en harina y luego en huevo batido, para ser posteriormente fritas en aceite hirviendo. En algunos casos se suele cocer la patata antes de ser cortada en rodajas. Las patatas fritas se suelen poner en una cazuela con agua (en ocasiones caldo) y se añade un majado en mortero de ajo, azafrán (en ocasiones se emplea un poco de pimentón), perejil y sal todo ello diluido a veces con una cierta cantidad de vino blanco. Se deja cocer y al final se suelen servir en una cazuela de barro con un poco de caldo. Se suele servir tras la elaboración.",
      urlImage: "https://lh5.googleusercontent.com/p/AF1QipPfJo1wDoPKTcfepuxkuaUbSvplLwXAjJOGySDd=w408-h306-k-no",
      location: {
          lat: -16.4003374,
          lon: -71.5500322,
      }
  },
  {
      id: 11,
      name: "Cachito's",
      shortDescription: "La experiencia Cachito´s es: hecho con amigos y para amigos. Alitas,hamburguesas y carnes todas en nuestra presentacion ahumada diseñado para tu diversión. (Recuerda que los medios de pago para recojo en tienda son: YAPE, LUKITA, TUNKI) ¡Cachito's, te lo llevamos a casa!",
      longDescription: "Es hoy! es hoy! Cumpliendo los protocolos definidos por nuestro gobierno hoy empezamos nuestro servicio de delivery, de 11:00 am a 7:00 pm! puedes realizar tus pedidos por la aplicación RAPPI o puedes recoger tu pedido en tienda con un 20% de DSCTO!. Ya puedes solicitar nuestro DELIVERY! Para ellos tienes dos opciones, a través de la aplicación Rappi o puedes realizar el recogo en tienda con 20% de descuento! (pagos para recojo en tienda con YAPE, LUKITA o TUNKI) No te olvides que puedes realizar tus pedidos de lunes a viernes de 11 am. a 7 pm. Sabemos que es una etapa muy difícil para todos, en especial para nosotros, las pequeñas empresas que tuvimos muchas dificultades para poder abrir.",
      urlImage: "https://lh5.googleusercontent.com/p/AF1QipP0y-yS-xWY0zG24wVaDAqTYMaoVfByrI0KzpoR=w427-h240-k-no",
      location: {
          lat: -16.4011721,
          lon: -71.5468021,
      }
  },
  {
      id: 12,
      name: "La Chuleta",
      shortDescription: "Restaurant Parrilleria La Chuleta Arequipeña, las mejores parrillas anticuchos y mollejitas Encuentranos en Av.Tahuaycani S/N Frente a La iglesia. También puedes realizar tus pedidos en nuestro local de huaranguillo.y seguir degustando de nuestras parrillas.",
      longDescription: "Es el lugar ideal para degustar una excelente comida con familiares y amigos, disfrutando del ambiente, el trato agradable y el estupendo servicio. En el interior se encuentran dos amplios salones decorados al estilo castellano y con capacidad para 50 personas cada uno, para comer a la carta y celebrar reuniones familiares. Los dos tienen horno, el de arriba es el antiguo horno pionero que todavía se usa, donde a la vista de los comensales se asa la carne. Sus comienzos fueron con piñones que el mismo recogía, limpiaba, y después tostaba en el horno que todavía hoy tenemos y que nos sigue proporcionando tan suculentos asados y carnes a la brasa, de los sarmientos o barañas, que en su día se cogían a mano y que hoy las maquinas nos preparan en bonitos paquetes.",
      urlImage: "https://lh5.googleusercontent.com/p/AF1QipPLIwL4Y3jqDWxf0fSfbz3qQE4SdWkiBLoBr5RM=w408-h306-k-no",
      location: {
          lat: -16.4061138,
          lon: -71.5573731,
      }
  },
  {
      id: 13,
      name: "La Casa De La Salchipapa",
      shortDescription: "Ofrecemos variedad de Salchipapas con bebidas y acompañamientos peruanos. Dirección: Calle Ricardo Palma 303, Umacollo, Arequipa.Papa peruana en una rica salchipapa, broster ó pollo a la brasa. Las mejores recetas con los mejores ingredientes hacen los mejores platos.",
      longDescription: "Los mejores momentos se comparten alrededor de una mesa. Al crear este restaurante, fué y es nuestra principal creencia, y a través de los años ha evolucionado hasta convertirse en lo que más soñamos cuando lo iniciamos UNA EXPERIENCIA. La salchipapa es un plato nacional, es para los peruanos lo que la hamburguesa es para los Estadounidenses. Si eres un gran fan de los embutidos, sabes que el mejor acompañamiento para salchipapa es la salchicha frankfurt. Ahumada en madera traida desde alemania con finas hierbas peruanas directo al plato con la mejor carne de cerdo y proceso insuperable. Exquisita, con crujientes papas fritas y salchicha frankfurt ahumada. Tenemos una amplia propuesta de variedad de salchipapas para un gusto exigente.",
      urlImage: "https://lh5.googleusercontent.com/p/AF1QipNRjeF1MpRf15cFuhMLRo1iNFbgRDARD7t0Ipx4=w426-h240-k-no",
      location: {
          lat: -16.4028649,
          lon: -71.5474413,
      }
  },
  {
      id: 14,
      name: "Pollería El Marqués",
      shortDescription: "Tenemos más de 20 años de experiencia y prestigio ofreciendo deliciosos pollos a la brasa al mejor precio. Visítanos y lo comprobarás.YA ESTAMOS AUTORIZADOS Volvemos con todas las medidas implementadas para llevarles nuestro sabor a sus hogares. CONSTANCIA DE REGISTRO DEL MINISTERIO DE SALUD:  Numero 006914-2020",
      longDescription: "El verdadero sabor de un pollo a la brasa bien hecho. El Nuevo Marqués, ubicado en el distrito de José Luis Bustamante y Rivero, de la ciudad de Arequipa, es un confortable y agradable local para disfrutar de la deliciosa cocina peruana. La especialidad emblemática del lugar son los tiernos y jugosos pollos a las brasas, que pueden ser pedidos por porciones de una mitad, un cuarto o un pollo entero, y se sirve acompañado de crujientes papas fritas y ensalada. El Nuevo Marqués se ha convertido en un lugar favorito de todo tipo de gente debido a su buena comida e informal atmósfera.Cocina peruana | Avenida Estados Unidos 311, José Luis Bustamante y Rivero, Arequipa (Arequipa)  Ver en mapa | precio medio entre S/ 20 / S/ 20",
      urlImage: "https://lh5.googleusercontent.com/p/AF1QipPugA7FDKqdtxi-UaxDrn94kc7TR51eg8d6Ivsu=w426-h240-k-no",
      location: {
          lat: -16.4013145,
          lon: -71.5472602,
      }
  },
  {
      id: 15,
      name: "Restaurant Punto y Sabor",
      shortDescription: "EL restaurante Ricuritas de la selva S.A.C, está orientada a satisfacer necesidades y preferencias de los consumidores en especial a turistas Nacionales y Extranjeros, para todas las edades, sexo, religión, etc. En cuanto al plan deimplementacion en el proyecto de Ricuritas de la Selva Restaurante S.A.C en el distrito de la Banda de Shilcayo",
      longDescription: "Desde el amazonas, gastronomía rural y simple. El Antojito de la Selva es uno de los tantos puestos de comida que encontrarás al pasear por la zona de Magdalena del Mar en la ciudad de Lima. Sin embargo, este pequeño restaurante tiene la particularidad de ofrecer comida típica de la zona amazónica del Perú, que no es muy común. En el Antojito de la Selva encontrarás las mejores yucas rellenas con un delicioso guiso de carne y aceitunas negras, acompañadas de una suave pasta de ají amarillo. Una parada obligada en Magdalena del Mar. Restaurante El Antojito de la Selva Cocina regional, peruana | Av. Bolognesi 152, Magdalena del Mar, Lima (Lima)  Ver en mapa | precio medio entre S/ 20 / S/ 20",
      urlImage: "",
      location: {
          lat: -16.4072744,
          lon: -71.5392452,
      }
  },
]

export default RestaurantsData