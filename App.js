import React from 'react'

// Navigation libraries
import 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';


// My Components
import tabNavigation from './app/navigation'
import login from './app/components/UserAuthentication/LoginForm/Login'
import register from './app/components/UserAuthentication/RegisterForm/Register'


// Setting up the main Stack navigation
const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={login}
          options={{ 
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Register"
          component={register}
          options={{ 
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Main"
          component={tabNavigation}
          options={{ 
            headerShown: false
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App;
