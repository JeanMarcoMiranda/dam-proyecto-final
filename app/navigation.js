import React from 'react'
import {
  StyleSheet,
  Button,
  TouchableOpacity
} from 'react-native'


// Navigation Libraries
import 'react-native-gesture-handler'
import { createStackNavigator } from '@react-navigation/stack'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'


// Icons
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

// MyComponents
import RestaurantList from './components/RestaurantsList/ListItems/List'
import RestaurantDetail from './components/RestaurantsList/DetailItem/Detail'
import RestaurantMap from './components/RestaurantsList/LocationItem/Location'
import RestaurantForm from './components/RestaurantsList/RestaurantForm/Form'
import RellenoUi from './components/Relleno/Relleno'
import Settings from './components/Configuracion/Settings'
import Cuenta from './components/Configuracion/Cuenta'

// Setting up the Stack navigation of each tab
const restaurantStack = createStackNavigator()

const restaurantInfoStackScreen = () => {
  return (
    <restaurantStack.Navigator>
      <restaurantStack.Screen
        name="Restaurants"
        component={RestaurantList}
        options={({navigation, route}) => ({
          title: "Restaurant List",
          headerStyle: {
            backgroundColor: '#142850'
          },
          headerTintColor: '#FFF',
          headerTitleAlign: 'center',
          headerRight: (props) => (
            <TouchableOpacity 
              style={{paddingRight: 10}}
              onPress={() => navigation.navigate('Form', { action: "Agregar Restaurante" })}  
            >
              <MaterialIcons name="add" color='white' size={30} />
            </TouchableOpacity>
          )
        })}
      />
      <restaurantStack.Screen
        name="Details"
        component={RestaurantDetail}
        options={{
          title: "Restaurant Detail",
          headerStyle: {
            backgroundColor: '#142850'
          },
          headerTintColor: '#FFF',
          headerTitleAlign: 'center',
        }}
      />
      <restaurantStack.Screen
        name="Maps"
        component={RestaurantMap}
        options={{
          title: "Restaurant Location",
          headerStyle: {
            backgroundColor: '#142850'
          },
          headerTintColor: '#FFF',
          headerTitleAlign: 'center'
        }}
      />

      <restaurantStack.Screen
        name="Form"
        component={RestaurantForm}
        options={{
          title: "Restaurant Form",
          headerStyle: {
            backgroundColor: '#142850'
          },
          headerTintColor: '#FFF',
          headerTitleAlign: 'center'
        }}
      />
    </restaurantStack.Navigator>
  )
}

const confiStack = createStackNavigator()
const confiStackScreen = ({route}) => {
  return(
    <confiStack.Navigator>
      <confiStack.Screen 
        name="Configuracion"
        component={Settings}
        initialParams={{aea: route.params.aea}}
        options={{
          title: "Configuracion",
          headerStyle: {
            backgroundColor: '#00BCFF',
          },
          headerTintColor: '#FFF',
          headerTitleAlign: 'center',
        }}
      />
      <confiStack.Screen 
        name="Cuenta"
        component={Cuenta}
        options={{
          title: "Cuenta",
          headerStyle: {
            backgroundColor: '#00BCFF',
          },
          headerTintColor: '#FFF',
          headerTitleAlign: 'center',
        }}
      />
    </confiStack.Navigator>
  )
}

// Setting up the bottom tab navigation

const Tab = createMaterialBottomTabNavigator()

const NavigationTab = ({ route }) => {

  const {userId} = route.params
  console.log(userId)

  return (
    <>
      <Tab.Navigator
        initialRouteName="Restaurants"
        activeColor='#fff'
        inactiveColor='#00bcff'
        barStyle={{ backgroundColor: '#142850', padding: 5 }}
        shifting='true'
      >
        <Tab.Screen
          name="Restaurants"
          component={restaurantInfoStackScreen}
          options={{
            tabBarLabel: 'Restaurants',
            tabBarIcon: ({ color, focused }) => (
              <MaterialIcons name="list" color={focused ? '#FFF' : '#00bcff'} size={20} />
            )
          }}

        />
        <Tab.Screen
          name="RellenoUi"
          component={RellenoUi}
          options={{
            tabBarLabel: 'Information',
            tabBarIcon: ({ color, focused }) => (
              <MaterialCommunityIcons name="food" color={focused ? '#FFF' : '#00bcff'} size={20} />
            )
          }}

        />

        <Tab.Screen
          name="Settings"
          component={confiStackScreen}
          initialParams={{aea: userId}}
          options={{
            tabBarLabel: 'Settings',
            tabBarIcon: ({ color, focused }) => (
              <MaterialIcons name="settings" color={focused ? '#FFF' : '#00bcff'} size={20} />
            )
          }}
        />
      </Tab.Navigator>
    </>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  }
})

export default NavigationTab