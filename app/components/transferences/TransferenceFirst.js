const moment = require('moment')
moment.locale('es')

import React, { useState } from 'react'
import {
  Text,
  View,
  Button,
  StyleSheet,
  TouchableWithoutFeedback,
  TextInput,
  Switch,
  Keyboard
} from 'react-native'

import ModalSelector from 'react-native-modal-selector'
import DateTimePickerModal from "react-native-modal-datetime-picker"

export default TransferenceFirst = ({ navigation }) => {
  //Stacte Variables
  const [originAccount, setOriginAccount] = useState('')
  const [destinyAccount, setDestinyAccount] = useState('')
  const [importe, setImporte] = useState('')
  const [reference, setReference] = useState('')
  const [date, setDate] = useState('')
  const [errors, setErrors] = useState([])

  //Control State Variables
  const [datePickerVisible, setDatePickerVisible] = useState(false);
  const [switchAble, setSwitchAble] = useState()
  const [modePicker, setModePicker] = useState('date')


  let index = 0
  const data = [
    { key: index++, section: true, label: 'Cuentas' },
    { key: index++, label: '000000000231455' },
    { key: index++, label: '000000000231456' },
    { key: index++, label: '000000000231457', accessibilityLabel: 'Tap here for cranberries' },
  ];

  //Auxiliar Functions
  const isEmpty = (variable) => {
    if (variable === '') {
      return true
    } else {
      return false
    }
  }

  const isValidNumber = (text) => {
    const isNumber = !isNaN(Number(text))
    if (isNumber) {
      return true
    } else {
      return false
    }
  }

  
  const showDateTimePicker = () => {
    // alert('showDateTimePicker');
     setDatePickerVisible(true);
     Keyboard.dismiss();
  };
 
  const hideDateTimePicker = () => {
    setDatePickerVisible(false);
  };

  const handleConfirm = (date) => {
    hideDateTimePicker()
    const fecha = moment(date).format('DD/MM/YYYY')
    setDate(fecha)
  };

  const changeSwitchValue = (value) => {
    setSwitchAble(value)
  }

  const cleanErrors = () => {
    setErrors([])
  }

  const sendData = () => {
    cleanErrors()
    const Data = [originAccount, destinyAccount, importe, reference, date]
    const emptyFields = Data.map((field, index) => {
      const emptyField = isEmpty(field)
      if (emptyField) {
        if(!errors.includes(`No pueden haber campos vacios`)){
          setErrors([...errors, `No pueden haber campos vacios`])
        }
      }
      return emptyField
    })

    if (!emptyFields.includes(true)) {
      const dataSend = {
        originAccount: originAccount,
        destinyAccount: destinyAccount,
        importe: importe,
        referencia: reference,
        fecha: date,
        mail: switchAble
      };
      if(isValidNumber(importe)){
        navigation.navigate('Second', dataSend)
      }else{
        if(!errors.includes(`El campo importe debe ser de tipo numerico.`)){
          setErrors([...errors, `El campo importe debe ser de tipo numerico.`])
        }
      }
    } 
  }

  return (
    <View style={style.formContainer}>
      <View style={style.formControl}>
        <Text style={{ paddingBottom: 10, }}>Cuenta Origen</Text>
        <ModalSelector
          data={data}
          initValue="Seleccione una cuenta"
          onChange={option => {
            setOriginAccount(option.label)
          }}
          cancelButtonAccessibilityLabel={'Cerrar'}
        >
          <TextInput
            style={style.Input}
            editable={false}
            placeholder="Seleccione una cuenta de origen"
            value={originAccount}
          />
        </ModalSelector>
      </View>

      <View style={style.formControl}>
        <Text style={{ paddingBottom: 10, }}>Cuenta Destino</Text>
        <ModalSelector
          data={data}
          initValue="Seleccione una cuenta"
          onChange={option => {
            setDestinyAccount(option.label)
          }}
          cancelText="Cerrar"
        >
          <TextInput
            style={style.Input}
            editable={false}
            placeholder="Seleccione una cuenta de origen"
            value={destinyAccount}
          />
        </ModalSelector>
      </View>

      <View style={style.formControl}>
        <Text style={{ paddingBottom: 10, }}>
          Importe
        </Text>
        <TextInput
          style={style.Input}
          placeholder="Ingrese el importe que desea realizar"
          value={importe}
          onChangeText={text => setImporte(text)}
        />
      </View>

      <View style={style.formControl}>
        <Text style={{ paddingBottom: 10, }}>
          Referencia
        </Text>
        <TextInput
          style={style.Input}
          placeholder="Ingrese una referencia"
          value={reference}
          onChangeText={text => setReference(text)}
        />
      </View>

      <View style={style.formControl}>
        <TouchableWithoutFeedback
          onPress={showDateTimePicker}>
          <View style={style.dateInputContainer}>
            <Text style={[style.Input, { width: '40%' }]}>
              {date}
            </Text>
          </View>
        </TouchableWithoutFeedback>

        <DateTimePickerModal
          isVisible={datePickerVisible}
          mode={modePicker}
          onConfirm={handleConfirm}
          onCancel={() => setDatePickerVisible(false)}
        />
      </View>

      <View style={style.formControl}>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          <View style={{ height: '100%', width: '50%', paddingTop: 2 }}>
            <Text style={{ textAlign: 'right' }}>
              Notificarme al correo
            </Text>
          </View>
          <View style={{ height: '100%', width: '30%' }}>
            <Switch
              style={{ width: '40%' }}
              onValueChange={changeSwitchValue}
              value={switchAble} />
          </View>
        </View>
      </View>

      {
        errors.length === 0 ? null : errors.map((error, index) => (
          <Text
            style={[style.Input, style.error, { opacity: 1 }]}
            key={index}>
            {error}
          </Text>
        )
        )
      }

      <View style={style.formControl}>
        <View style={{flex: 1, alignItems:'center'}}>
          <Button
            style={{width: 200}}
            title="Go Next"
            onPress={sendData}
          />
        </View>
      </View>
    </View>
  )
}

const style = StyleSheet.create({
  formContainer: {
    padding: 20
  },
  formControl: {
    marginTop: 10,
    marginBottom: 10,
    height: 70
  },
  dateInputContainer: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    padding: 10,
  },
  Input:{
    height: 40,
    backgroundColor: '#E7E7E7',
    textAlign: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'grey',
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 15,
  },
  error: {
    borderWidth: 0,
    padding: 7,
    color: '#FF410D',
    alignItems: 'center',
    backgroundColor: '#F5B7B1'
  }
})