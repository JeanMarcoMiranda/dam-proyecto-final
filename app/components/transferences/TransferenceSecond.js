const moment = require('moment')
moment.locale('es')

import React, { useState } from 'react'
import { Text, View, Button, StyleSheet, TextInput } from 'react-native'

export default TransferenceSecond = ({ navigation, route }) => {
  const [Importe, setImporte] = useState(0)
  const { originAccount, destinyAccount, importe, referencia, fecha, mail } = route.params
  return (
    <View style={{ flex: 1, justifyContent: 'space-around', padding: 50 }}>
      <View >
        <View>
          <View style={styles.form}>
            <Text>Cuenta origen</Text>
            <TextInput editable={false}
              placeholder="Cuenta origen"
              selectionColor={"#428Af8"}
              style={styles.input}
              value={originAccount}
            />
          </View>

          <View style={styles.form}>
            <Text>Cuenta destino</Text>
            <TextInput editable={false}
              placeholder="Cuenta destino"
              selectionColor={"#428Af8"}
              style={styles.input}
              value={destinyAccount}
            />
          </View>

          <View style={styles.form}>
            <Text>Importe</Text>
            <TextInput editable={false}
              placeholder="Importe"
              selectionColor={"#428Af8"}
              style={styles.input}
              value={importe}
            />
          </View>

          <View style={styles.form}>
            <Text>Referencia</Text>
            <TextInput editable={false}
              placeholder="Referencia"
              selectionColor={"#428Af8"}
              style={styles.input}
              value={referencia}
            />
          </View>

          <View style={styles.form}>
            <Text>Fecha</Text>
            <TextInput editable={false}
              placeholder="Fecha"
              selectionColor={"#428Af8"}
              style={styles.input}
              value={fecha}
            />
          </View>

          <View style={styles.form}>
            <Text>Correo</Text>
            <TextInput editable={false}
              placeholder="Correo"
              selectionColor={"#428Af8"}
              style={styles.inputReserva}
              value={mail ? "Si" : "No"}
            />
          </View>

          <View style={styles.botonContainer}>
            <View style={styles.boton}>
              <Button title="Volver" onPress={() => navigation.navigate('First')}> </Button>
            </View>
            <View style={styles.boton}>
              <Button title="Confirmar" onPress={() => navigation.navigate('Third')}> </Button>
            </View>
          </View>

        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  form: {
    paddingBottom: 10
  },

  input: {
    borderBottomWidth: 1,
    height: "auto",
    borderBottomColor: "#D6D6D6",
  },

  inputReserva: {
    borderBottomWidth: 1,
    height: "auto",
  },

  botonContainer: {
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: "flex-start",
    margin: 15,
  },

  boton: {
    width: '50%',
    padding: 5
  },

  itemCont: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 20,
    justifyContent: "flex-start",
    margin: 15,
  },
});