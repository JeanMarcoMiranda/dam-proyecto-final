const moment = require('moment')
moment.locale('es')

import React, {useState} from 'react'
import {Text, View, Button, Image, StyleSheet} from 'react-native'

export default TransferenceThird = ({navigation}) => {
  const [importe, setImporte] = useState(0)

  return(
    <View style={{flex:1, justifyContent:'space-around', padding:50}}>

                <View style={styles.form}>
                    <Image
                    source={{uri: 'https://cdn.icon-icons.com/icons2/1506/PNG/512/emblemok_103757.png'}}
                    style={styles.image}
                    />
                </View>
                <View style={styles.boton}>
                    <Button title="Aceptar" onPress={ () => navigation.navigate('First')}> </Button>
                </View>

            </View>
  )
}

const styles = StyleSheet.create({
  form:{
      justifyContent: 'center',
      alignItems: 'center',
  },

  image: {
      width: "50%",
      height: "50%",
  },

  boton:{
      width: '100%',
  },
});