import firebase from '../../../../firebase'
import 'firebase/firestore'

import React, { useState } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native'

const RegisterForm = ({navigation}) => {
  /*============ Setting our state ============*/
  /* Functional variables */
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const onPress = async () => {
    console.warn("Clicked")
    if(name === ''){
      alert('You have to fill the name at least') 
    }else{

      const db = firebase.firestore()
      db.settings({experimentalForceLongPolling: true})
    
      await db.collection('users').add({
        name,
        email,
        password,
      })

      setName('')
      setEmail('')
      setPassword('')
      navigation.navigate('Login')

    }
  }

  


  return (
    <View style={styles.padre}>
      <View style={styles.container}>
        <Text style={styles.title}>
          Register
        </Text>
        <View style={{width: '100%', marginTop: 20}}>
          <TextInput
            style={styles.input}
            placeholder="Name"
            placeholderTextColor="grey"
            defaultValue={name}
            onChangeText={text => setName(text)}
          />
          <TextInput
            style={styles.input}
            placeholder="Email"
            placeholderTextColor="grey"
            defaultValue={email}
            onChangeText={text => setEmail(text)}
          />
          <TextInput
            style={styles.input}
            placeholder="Password"
            placeholderTextColor="grey"
            secureTextEntry={true}
            defaultValue={password}
            onChangeText={text => setPassword(text)}
          />
        </View>

        <View style={{flex: 1, alignItems: 'center'}}>
          <TouchableOpacity
            style={[styles.button]}
            onPress={() => onPress()}
          >
            <Text
              style={[styles.button, styles.buttonText]}
            >
              Sign Up
                      </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  padre: {
    width: '100%',
    height: '100%',
    backgroundColor: '#142850',
    paddingHorizontal: '5%',
    paddingVertical: '25%',
  },
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    width: '100%',
    padding: 15,
    fontWeight: 'bold',
    borderBottomWidth: 4,
    borderBottomColor: '#27496D'
  },
  input: {
    borderBottomWidth: 2,
    borderBottomColor: '#DAE1E7',
    marginBottom: 35,
    fontSize: 20,
    width: '100%',
    color: 'white'
  },
  button:{
    backgroundColor: '#00909E',
    paddingVertical: 5,
    borderRadius: 10,
    width: 200,
    alignItems: 'center',
  },
  buttonText:{
    color: '#FFF',
    textAlign: 'center',
    fontStyle: 'normal',
    fontSize: 20
  }
})

export default RegisterForm