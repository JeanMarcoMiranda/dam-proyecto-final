/* eslint-disable react-native/no-inline-styles */
import firebase from '../../../../firebase';
import 'firebase/firestore';

import React, { useState } from 'react'
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Text,
  View,
  Image,
} from 'react-native';

const LoginForm = ({navigation}) => {
  /*============ Setting our state ============*/
  /* Functional variables */
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  /* Style variables */
  const [emailInputStyle, setEmailInputStyle] = useState(styles.input);
  const [passwordInputStyle, setPasswordInputStyle] = useState(styles.input);


  /* Setting our functions */
  const onPressLogin = () => {

    const db = firebase.firestore();
    db.settings({experimentalForceLongPolling: true});

    const ref = db.collection('users');

    ref
      .where('email', '==', email)
      .where('password', '==', password)
      .get()
      .then(snapshot => {
        if (snapshot.empty) {
          alert('Usuario no encontrado');
          return;
        }

        snapshot.forEach(doc => {
          navigation.navigate('Main', {userId: doc.id});
        });
      })

      .catch(err => {
        console.log('Error getting documents', err);
      });
    /* navigation.navigate('Main') */
  };



  return (
    <View style={styles.padre}>
      <View style={styles.container}>
        <Image
          style={{
            width: 150,
            height: 150,
          }}
          source={require('../../../../assets/img/react.png')}
        />
        <Text style={{marginBottom: 15, fontSize: 30, color: '#fff', fontWeight: 'bold'}}>
          Sign In
        </Text>
        <TextInput
          style={emailInputStyle}
          placeholder="Email"
          placeholderTextColor="#fff"
          defaultValue={email}
          onFocus={() => setEmailInputStyle(styles.inputFocused)}
          onBlur={() => setEmailInputStyle(styles.input)}
          onChangeText={text => setEmail(text)}
        />

        <TextInput
          style={passwordInputStyle}
          onFocus={() => setPasswordInputStyle(styles.inputFocused)}
          onBlur={() => setPasswordInputStyle(styles.input)}
          placeholder="Password"
          placeholderTextColor="#fff"
          secureTextEntry={true}
          defaultValue={password}
          onChangeText={text => setPassword(text)}
        />

        {/* {
                    this.state.error.length === 0 ? null : this.state.error.map((error, index) => (
                        <Text
                            style={[styles.input, styles.error, { opacity: 1 }]}
                            key={index}>
                            {error}
                        </Text>
                    )
                    )
                } */}

        <TouchableOpacity
          style={[styles.button]}
          onPress={() => onPressLogin()}>
          <Text style={[styles.button, styles.buttonText]}>Ingresar</Text>
        </TouchableOpacity>
        
        <View style={{ flex : 1, flexDirection : 'row', width: '100%', justifyContent: 'center', alignItems: 'center'}}>
          <Text style={[styles.buttonText, {padding: 0, fontSize: 15}]}>
            Aún no tiene una cuenta?
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('Register')}>
            <Text style={[styles.buttonText, {color: '#71E9F7', fontSize: 15}]}>Registrarse</Text>
          </TouchableOpacity>
        </View>
        
      </View>
    </View> 
  );
}

const styles = StyleSheet.create({
  padre: {
    width: '100%',
    height: '100%',
    backgroundColor: '#142850',
    paddingHorizontal: '5%',
    paddingVertical: '25%',
  },
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
  },
  input: {
    height: 50,
    width: '100%',
    backgroundColor: '#27496D',
    marginBottom: 15,
    marginTop: 15,
    paddingLeft: 25,
    borderWidth: 2,
    borderColor: '#00909E',
    borderRadius: 15,
    fontSize: 20,
    color: '#fff',
    opacity: 0.7,
  },

  inputFocused: {
    height: 50,
    width: '100%',
    marginBottom: 15,
    marginTop: 15,
    paddingLeft: 25,
    borderWidth: 2,
    borderColor: '#DAE1E7',
    backgroundColor: '#27496D',
    borderRadius: 15,
    fontSize: 20,
    color: '#fff',
    opacity: 0.7,
  },

  button: {
    width: 200,
    marginTop: 25,
    backgroundColor: '#00909E',
    borderRadius: 30,
  },
  buttonText: {
    marginTop: 0,
    padding: 10,
    color: 'white',
    textAlign: 'center',
    fontSize: 20
  },

  error: {
    borderWidth: 0,
    padding: 7,
    color: '#FF410D',
    alignItems: 'center',
    backgroundColor: '#F5B7B1',
  },
});

export default LoginForm