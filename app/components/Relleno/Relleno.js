/* aea PENSA EN ALGO WACHO :v */
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    SafeAreaView,
    TextInput,
    ScrollView,
    Image,
    Dimensions,
    FlatList
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

const { height, width } = Dimensions.get('window')

const Category = (props) => {
    return(
        <View style={{ height: 130, width: 130, marginLeft: 20, borderWidth: 0.5, borderColor: '#808080', backgroundColor: 'white' }}>
            <View style={{ flex: 2 }}>
                <Image
                    source={{
                        uri: `${props.imageUri}`
                    }}
                    style={{ flex: 1, width: null, height: null, resizeMode: 'cover' }}
                />
            </View>
            <View style={{ flex: 1, paddingLeft: 10, paddingTop: 10}}>
                <Text style={{fontSize: 12, color: '#7FC057'}}>{props.name}</Text>
                <Text style={{fontSize: 10, color: '#0083D0'}}>{props.descripcion}</Text>
            </View>
        </View>
    )
}

const RellenoUi = ({navigation}) => {
    return(
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <View style={{ height: 100, backgroundColor: '#27496D', borderBottomWidth: 1, borderBottomColor: '#dddddd' }}>
                    <View style={{
                        flexDirection: 'row', padding: 10,
                        backgroundColor: 'white', marginHorizontal: 20,
                        shadowOffset: { width: 0, height: 0 },
                        shadowColor: 'black',
                        shadowOpacity: 0.2,
                        elevation: 1,
                        marginTop: 25
                    }}>
                        <Icon name="ios-search" size={20} style={{ marginRight: 10, marginTop: 5 }} />
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Buscador"
                            placeholderTextColor="grey"
                            style={{ flex: 1, fontWeight: '700', backgroundColor: 'white', height: 35 }}
                        />
                    </View>
                </View>
                <ScrollView
                    scrollEventThrottle={16}
                    >
                    <View style={{ flex: 1, backgroundColor: '#DDDDDD', paddingTop: 20 }}>
                        <Text style={{ fontSize: 24, fontWeight: '700', paddingHorizontal: 20 }}>
                            Top 5 Restaurantes venta del año
                        </Text>

                        <View style={{ height: 130, marginTop: 20 }}>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri="https://i.pinimg.com/originals/be/29/ca/be29cad23ab4eee0396788561335aeb2.jpg" /* imageUri={require('../assets/home.jpg')} https://steamcdn-a.akamaihd.net/steam/apps/985080/header.jpg?t=1576593671*/
                                          name="1° PizzasDonPEPA"
                                          descripcion="al toque mi rey"
                                />
                                <Category imageUri="https://images5.alphacoders.com/444/thumb-1920-444393.jpg" /* imageUri={require('../assets/experiences.jpg')} */
                                          name="2° ChichenitzaPizza"
                                          descripcion="de noche mi pana"
                                />
                                <Category imageUri="https://e00-expansion.uecdn.es/assets/multimedia/imagenes/2019/06/25/15614775255199.jpg" /* imageUri={require('../assets/home.jpg')} https://steamcdn-a.akamaihd.net/steam/apps/985080/header.jpg?t=1576593671*/
                                          name="3° Quiero mi 20 profe"
                                          descripcion="te gusta la hierba?"
                                />
                                <Category imageUri="https://www.lasdunashotel.com/uploads/1/1/2/9/112964637/restaurante-achirana-dunas-1400x700_1.jpg" /* imageUri={require('../assets/home.jpg')} https://steamcdn-a.akamaihd.net/steam/apps/985080/header.jpg?t=1576593671*/
                                          name="4° Butos, Enchiladas"
                                          descripcion="piscina en la playa"
                                />
                                <Category imageUri="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT5RCRRv5f4Mvgx1k11ValTc8iU6CBdYef2ig&usqp=CAU" /* imageUri={require('../assets/restaurant.jpg')} */
                                          name="5° Piscineros"
                                          descripcion="aea manito"
                                />
                            </ScrollView>
                        </View>
                        <View style={{ marginTop: 25, paddingHorizontal: 20, backgroundColor: '#F53158', marginRight: 15, marginLeft: 15, borderRadius: 10, paddingVertical: 12 }}>
                            <Text style={{ fontSize: 24, fontWeight: '700', color: 'white' }}>
                                Introducete al mundo culinario
                            </Text>
                            <Text style={{ fontWeight: '100', marginTop: 10, color: 'white' }}>
                                Posicionate en las primera opciones del buscador
                            </Text>
                            <View style={{ width: width - 70, height: 200, marginTop: 20 }}>
                                <Image
                                    style={{ flex: 1, height: null, width: null, resizeMode: 'cover', borderRadius: 5, borderWidth: 1, borderColor: '#dddddd' }}
                                    source={{
                                        uri: 'https://5.imimg.com/data5/YS/TA/MY-13049124/restaurant-wallpaper-design-500x500.jpg'
                                    }}
                                />
                            </View>
                            <View style={{backgroundColor: 'white', margin: 6, borderRadius: 12,}}>
                                <Text style={{ marginLeft: 13, marginRight: 4, marginTop: 5, textAlign:'justify' }}>
                                    Buenas Wachos, quieren ser numero 1 en nuestra aplicacion?, bueno primero prepara tu billetera porque nosotros solo aceptamos paypal.
                                </Text>
                                <Text style={{ marginLeft: 13, marginRight: 4, marginTop: 5, textAlign:'justify', marginBottom: 5 }}>
                                    Ahora debes darme todo tu dinero o mañana tu perro amanecerá en el chifa de la esquina. :dagger:
                                </Text>
                            </View>
                        </View>
                        <View style={{ marginTop: 20, paddingHorizontal: 20}}>
                            <FlatList
                                data={[
                                    {key:'a', name: 'Italy Pizza', description:'Accede al sitio web para consultar la disponibilidad y el precio más reciente.', imageUri:'https://i.pinimg.com/736x/ba/69/1d/ba691dd6adb90dab630962fe114979a7.jpg'},
                                    {key:'b', name: 'Burger French', description:'Hola4', imageUri:'https://i.pinimg.com/originals/40/89/a5/4089a577fc40aed4e89ba5430fd066bd.jpg'},
                                    {key:'c', name: 'Ramen Snack', description:'Hola4', imageUri:'https://www.dhresource.com/0x0/f2/albu/g10/M01/0D/E1/rBVaWV1CuKOAH1dMAATPF2RbfRs175.jpg'},
                                    {key:'d', name: 'Casual Bar', description:'Hola4', imageUri:'https://ae01.alicdn.com/kf/HTB1g7uYNXXXXXanaFXXq6xXFXXXM/Foto-de-papel-de-comida-japonesa-estilo-del-papel-pintado-restaurante-casual-Bar-Caf-tienda-de.jpg'}]}
                                renderItem={({ item }) =>
                                    <View style={{ borderRadius:10, backgroundColor: '#4169E1',flex: 1, height: 130, width: null, borderWidth: 3, borderColor: '#dddddd', flexDirection: 'row'}}>
                                        <Image
                                            source={{
                                                uri: `${item.imageUri}`
                                            }}
                                            style={{ flex: 1, width: 100, height: 100, resizeMode: 'cover', margin: 15 }}
                                        />
                                        <View style={{ flex:1, justifyContent: 'center'}}>
                                            <Text style={{ fontSize: 18, color: 'white', marginBottom:15}}>
                                                {item.name}
                                            </Text>
                                            <Text style={{ fontSize: 15, color: 'white', marginBottom:15}}>
                                                {item.description}
                                            </Text>
                                        </View>
                                    </View>
                                }
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //backgroundColor: 'brown',/*none*/
        alignItems: 'center',
        justifyContent: 'center',
    },
    aea1ScrollView: {
        //backgroundColor: 'white',/*white*/
        height: 'auto',
        width: '100%', /*auto*/
        margin: 10
    },
    aea1View: {
        flex: 1,
        backgroundColor: 'white',/*white*/
        paddingTop: 20,
    },
    aea2View: {
        height: 180,
        marginTop: 20,
        //backgroundColor: 'green',/*none*/
    },
    aea3View: {
        height: 170,
        width: "100%",
        //backgroundColor: 'yellow',/*none*/
    },
    aea4View: {
        flex: 2,
        //backgroundColor: 'yellow',/*none*/
    },
    aea5View: {
        flex: 1,
        paddingLeft: 10,
        paddingTop: 10,

    },
    aea1Image: {
        flex:1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
    aea1Text: {
        fontSize: 24,
        fontWeight: '700',
        paddingHorizontal: 20,
    },
});

export default RellenoUi