import firebase from '../../../firebase'
import 'firebase/firestore'

import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native'

const Cuenta = ({ navigation, route }) => {

   const { userId } = route.params

   const [name, setName] = useState('')
   const [email, setEmail] = useState('')
   const [password, setPassword] = useState('')

   const getData = async () => {
      const db = firebase.firestore()
      db.settings({ experimentalForceLongPolling: true })

      const doc = await db.collection('users').doc(userId).get()
      if(!doc.exists) {
         console.log('No hay ningun usuario con esos datos')
      } else {
         const nombre = doc.data().name
         const emails = doc.data().email
         setName(nombre)
         setEmail(emails)
      }
   }

   useEffect(() => {
      getData()
      return () => {
         setName('')
         setEmail('')
         setPassword('')
       }
   }, [])

   const onPress = async () => {
      console.warn("Clicked")
      if(name === ''){
         alert('You have to fill the name at least') 
      }else if(password === ''){
         alert('You have to fill the password at least') 
      } else {
         const db = firebase.firestore()
         db.settings({experimentalForceLongPolling: true})
         let ref = db.collection('users')
         await ref.doc(userId).set({
            name,
            email,
            password,
         })

         setName('')
         setEmail('')
         setPassword('')
         navigation.popToTop()

      }
   }

   return (
      <View style={styles.padre}>
        <View style={styles.container}>
          <Text style={styles.title}>
            Editar Datos
          </Text>
          <View style={{width: '100%', marginTop: 20}}>
            <TextInput
              style={styles.input}
              placeholder="Name"
              placeholderTextColor="grey"
              defaultValue={name}
              onChangeText={text => setName(text)}
            />
            <TextInput
              style={styles.input}
              placeholder="Email"
              placeholderTextColor="grey"
              defaultValue={email}
              onChangeText={text => setEmail(text)}
            />
            <TextInput
              style={styles.input}
              placeholder="Password"
              placeholderTextColor="grey"
              secureTextEntry={true}
              defaultValue={password}
              onChangeText={text => setPassword(text)}
            />
          </View>
  
          <View style={{flex: 1, alignItems: 'center'}}>
            <TouchableOpacity
              style={[styles.button]}
              onPress={() => onPress()}
            >
              <Text
                style={[styles.button, styles.buttonText]}
              >
                Actualizar
                        </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
}

const styles = StyleSheet.create({
   padre: {
     width: '100%',
     height: '100%',
     backgroundColor: '#142850',
     paddingHorizontal: '5%',
     paddingVertical: '25%',
   },
   container: {
     flex: 1,
     height: '100%',
     width: '100%',
     justifyContent: 'center',
     alignItems: 'center',
     padding: 30,
   },
   title: {
     textAlign: 'center',
     fontSize: 30,
     color: 'white',
     width: '100%',
     padding: 15,
     fontWeight: 'bold',
     borderBottomWidth: 4,
     borderBottomColor: '#27496D'
   },
   input: {
     borderBottomWidth: 2,
     borderBottomColor: '#DAE1E7',
     marginBottom: 35,
     fontSize: 20,
     width: '100%',
     color: 'white'
   },
   button:{
     backgroundColor: '#00909E',
     paddingVertical: 5,
     borderRadius: 10,
     width: 200,
     alignItems: 'center',
   },
   buttonText:{
     color: '#FFF',
     textAlign: 'center',
     fontStyle: 'normal',
     fontSize: 20
   }
 })

export default Cuenta
