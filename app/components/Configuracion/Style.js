import { StyleSheet } from "react-native"

const Style = StyleSheet.create({
   loginContainer:{
      flex:1,
      backgroundColor:'#dddddd',
   },
   loginImageContainer:{
      marginVertical: 130,
      alignItems: 'center',
   },
   loginLogo:{
      height: 200,
      width: 400,
      borderRadius: 15,
   },
   loginInputContainer:{
      height: 150,
   },
   loginInput:{
      backgroundColor: '#E62429',
      marginHorizontal: 20,
      marginVertical: 10,
   },
   loginButton:{
      marginVertical: 20,
      alignSelf: 'center',
      width: 249,
      paddingVertical: 8,
      backgroundColor: '#151515',
      borderWidth: 2,
      borderColor:'#E62429',
      borderRadius: 10,
   },
   loginButtonText:{
      fontSize: 20,
      color: 'white',
      textAlign: 'center',
   },
   listContainer:{
      flex:1,
      backgroundColor: '#202020'
   },
   itemSeparator:{
      height: 3,
      width: '100%',
      alignSelf: 'center',
      backgroundColor: '#D6000A',
   },
   itemContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      marginVertical: 8,
   },
   itemImageContainer: {
      marginLeft: 5,
      width: 95,
      height: 95,
      borderRadius: 17,
      backgroundColor: '#151515',
   },
   itemImage:{
      height: 90,
      width: 90,
      borderRadius: 20,
      alignSelf: 'flex-end',
   },
   itemDescContainer:{
      marginLeft:10
   },
   itemTitle:{
      width: 270,
      borderTopRightRadius: 25,
      borderBottomLeftRadius: 25,
      backgroundColor: '#151515',
      paddingVertical: 2,
      textAlign: 'center',
      fontSize: 22,
      color: 'white',
      fontWeight: 'bold',
   },
   itemSub:{
      fontSize: 15,
      paddingVertical: 3,
      color: 'white',
   },
   eventText:{
      fontSize: 30,
      color: 'white',
   },
   detailScrollView:{
      backgroundColor: '#202020',
      flex: 1,
   },
   detailImage:{
      width: 393,
      height: 300,
      borderBottomLeftRadius: 45,
      borderBottomRightRadius: 45,
   },
   heroDescContainer:{
      marginVertical: 15,
      marginHorizontal: 21,
   },
   heroDesc:{
      borderTopRightRadius: 25,
      borderBottomLeftRadius: 25,
      paddingVertical: 8,
      paddingHorizontal: 18,
      textAlign: 'justify',
      color:'white',
      backgroundColor: '#151515',
   },
   detailSubContainer:{
      backgroundColor: '#00bcff',
      paddingTop: 10,
   },
   detailSubTitle:{
      color: '#142850',
      textAlign: 'center',
      fontSize: 33,
      marginBottom: 10,
   },
   detailSubDeco:{
      height: 4,
      width: '100%',
      alignSelf: 'center',
      backgroundColor: '#27496d',
   },
   comicContainer:{
      marginVertical: 10,
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
   },
   comicImageContainer: {
      flex: .42,
      paddingHorizontal: 10,
   },
   comicImage:{
      width: 156,
      height: 250,
   },
   comicDescContainer:{
      flex: .58,
      marginHorizontal: 10,
      borderTopRightRadius: 25,
      borderBottomLeftRadius: 25,
      backgroundColor: '#151515',
   },
   comicTitle:{
      paddingTop: 8,
      paddingHorizontal: 10,
      color:'white',
      textAlign: 'center',
      marginBottom: 5,
   },
   comicDesc:{
      color:'white',
      textAlign: 'justify',
      paddingBottom: 8,
      paddingHorizontal: 10,
   },
   detailOtherContainer:{
      flexDirection: 'row',
      justifyContent: 'flex-start',
      margin: 5,
   },
   otherButton:{
      margin: 5,
      alignSelf:'center',
      backgroundColor: '#E62429',
      borderTopRightRadius: 15,
      borderBottomLeftRadius: 15,
   },
   otherButtonText:{
      textAlign: 'center',
      width:200,
      paddingVertical: 4,
      paddingHorizontal: 8, 
      fontSize: 20,
      color: 'white',
   },
   otherContainer:{
      flex: .65
   },
   otherImageContainer: {
      flex: .45,
      alignItems: 'center',
   },
   otherImageBorder: {
      backgroundColor: '#151515',
      marginVertical: 2,
      padding: 9,
      borderRadius: 10,
   },
   otherImage: {
      width: 110,
      height: 110,
   },
   itemSetting: {
      marginVertical: 12,
      backgroundColor: "#27496d",
      width: '90%',
      borderRadius: 15,
      alignSelf: 'center'
   },
})

export default Style