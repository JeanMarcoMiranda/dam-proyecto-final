import React, {Component} from 'react'
import { TouchableOpacity, FlatList, Image, Text, View } from 'react-native';
import Style from './Style'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

const options = [
   {
      id: 1,
      text: 'Account',
      iconName: 'account'
   },
   {
      id: 2,
      text: 'Privacy and Security',
      iconName: 'shield-check'
   },
   {
      id: 3,
      text: 'Connections',
      iconName: 'signal-variant'
   },
   {
      id: 4,
      text: 'Voice and Video',
      iconName: 'video'
   },
   {
      id: 5,
      text: 'Appearance',
      iconName: 'store'
   },
   {
      id: 6,
      text: 'Notifications',
      iconName: 'bell'
   },
]

function Item({title, iconName, aea, navi}) {
   return(
      <TouchableOpacity style={Style.itemSetting} onPress={() => {
         if(title == "Account") {
            console.log(aea + ' To Account')
            navi.navigate('Cuenta', { userId: aea})
         }
      }}>
         <View style={[Style.itemDescContainer,{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginVertical: 8,
         }]}>
            <MaterialCommunityIcons name={iconName} color={'white'} size={35} />
            <Text style={[Style.eventText,{marginLeft: 15}]}>{title}</Text>
         </View>
      </TouchableOpacity> 
   )
}

class Settings extends Component {
   constructor(props) {
      super(props)
      this.state = {
         userId: this.props.route ? this.props.route.params.aea : ''
      }
   }

   FlatListSeparator = () => {
      return(
         <View style={[Style.itemSeparator,{backgroundColor:"#142850", width:'93%'}]} />
      )
   }

   render() {
      return(
         <View style={Style.loginContainer}>
            <View style={{backgroundColor: '#142850', paddingVertical: 15, alignItems: 'center'}}>
               <Image source={{
                  uri: 'https://steamcdn-a.akamaihd.net/steam/apps/985080/header.jpg?t=1576593671'
               }} style={Style.loginLogo}/>
            </View>            
            <View style={Style.detailSubContainer}>
            </View>
               <View style={Style.detailSubDeco} />
            <FlatList
               data={options.length > 0 ? options : []}
               keyExtractor={item => item.id}
               ItemSeparatorComponent={this.FlatListSeparator}
               renderItem={({item}) => (
                  <Item 
                     title={item.text}
                     iconName={item.iconName}
                     aea={this.state.userId}
                     navi={this.props.navigation}
                  />
               )}
               style={{marginTop:10}}
            />
         </View>
      )
   }
}

export default Settings