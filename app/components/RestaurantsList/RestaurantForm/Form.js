import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Button,
  ScrollView,
  Alert,
} from 'react-native'
import firebase from '../../../../firebase'
import 'firebase/firestore'

const RestaurantForm = ({ route, navigation}) => {

  const [restaurantData, setRestaurantData] = useState((route.params.item) ? route.params.item : {})

  const onPressButton = () => {
    Alert.alert(
      "Confirmación",
      "¿Estas conforme con los datos ingresados?",
      [
        {
          text: 'No',
          onPress: () => console.log('Cancelado'),
          style: 'cancel'
        },
        {
          text: 'Si',
          onPress: () => {
            if(route.params.action === "Agregar Restaurante"){
              onPressCrearButton()
            } else {
              onPressActualizarButton()
            }
          }
        }
      ]
    )
  }

  const onPressActualizarButton = async () => {
    try{
      const db = firebase.firestore()
      db.settings({ experimentalForceLongPolling: true })
      const restaurantRef = db.collection('restaurants')
      await restaurantRef.doc(restaurantData.id).set({
        name: restaurantData.name,
        shortDescription: restaurantData.shortDescription,
        longDescription: restaurantData.longDescription,
        urlImage: restaurantData.urlImage,
        location: restaurantData.location
          ? restaurantData.location
          : {lat: -16.4046245, lon: -71.5477197},
      });

      navigation.navigate('Restaurants');

    } catch (error) {
      console.warn('Ha ocurrido un error al guardar los datos');
    }
  };

  const onPressCrearButton = async () => {
    try {
      const db = firebase.firestore();
      db.settings({experimentalForceLongPolling: true});

      const restaurantRef = db.collection('restaurants');
      await restaurantRef.add({
        name: restaurantData.name,
        shortDescription: restaurantData.shortDescription,
        longDescription: restaurantData.longDescription,
        urlImage: restaurantData.urlImage
          ? restaurantData.urlImage
          : 'https://lh5.googleusercontent.com/p/AF1QipNRjeF1MpRf15cFuhMLRo1iNFbgRDARD7t0Ipx4=w426-h240-k-no',
        location: restaurantData.location
          ? restaurantData.location
          : {lat: -16.4046245, lon: -71.5477197},
      });

      navigation.navigate('Restaurants');

    } catch (error) {
      console.log(error);
      console.warn('Ha ocurrido un error al guardar los datos');
    }
  };

  return (
    <ScrollView style={style.formContainer}>
      <View style={style.formControl}>
        <Text style={{paddingBottom: 10, fontWeight: 'bold', fontSize: 20}}>Imagen</Text>
        <TextInput
          style={style.Input}
          placeholder="Direccion de la Imagen"
          editable={false}
          defaultValue={
            restaurantData.urlImage ||
            'https://r-cf.bstatic.com/images/hotel/max1024x768/177/177657398.jpg'
          }
        />
      </View>

      <View style={style.formControl}>
        <Text style={{paddingBottom: 10, fontWeight: 'bold', fontSize: 20}}>Nombre</Text>
        <TextInput
          style={style.Input}
          placeholder="Ingrese el nombre de su establecimiento"
          defaultValue={restaurantData.name}
          onChangeText={(text) => {
            const cambioObjeto = {...restaurantData, name: text};

            setRestaurantData(cambioObjeto);
          }}
        />
      </View>

      <View style={style.formControl}>
        <Text style={{paddingBottom: 10, fontWeight: 'bold', fontSize: 20}}>Descripción Corta</Text>
        <View style={style.textAreaContainer}>
          <TextInput
            style={[style.textArea, {height: 70}]}
            multiline={true}
            numberOfLines={4}
            placeholder="Ingrese una descripcion detallada de su establecimiento."
            defaultValue={restaurantData.shortDescription}
            onChangeText={(text) => {
              const cambioObjeto = {...restaurantData, shortDescription: text};

              setRestaurantData(cambioObjeto);
            }}
          />
        </View>
      </View>

      <View style={style.formControl}>
        <Text style={{paddingBottom: 10, fontWeight: 'bold', fontSize: 20}}>Descripción Detallada</Text>
        <View style={style.textAreaContainer}>
          <TextInput
            style={[style.textArea, {height: 170}]}
            multiline={true}
            numberOfLines={6}
            placeholder="Ingrese una descripcion detallada de su establecimiento."
            defaultValue={restaurantData.longDescription}
            onChangeText={(text) => {
              const cambioObjeto = {...restaurantData, longDescription: text};

              setRestaurantData(cambioObjeto);
            }}
          />
        </View>
      </View>

      <View style={style.formControl}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Button
            style={{width: 200}}
            title={route.params.action}
            onPress={() => onPressButton()}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const style = StyleSheet.create({
  formContainer: {
    paddingHorizontal: 20,
    paddingVertical: 40
  },
  formControl: {
    marginTop: 5,
    marginBottom: 5,
  },
  Input: {
    height: 40,
    textAlign: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#D8DADB',
    marginBottom: 15,
    backgroundColor:'white',
    color: '#142850',
    fontSize: 15,
  },

  textAreaContainer: {
    textAlign: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#D8DADB',
    marginBottom: 15,
  },

  textArea: {
    flex: 0,
    justifyContent: 'flex-start',
    color: '#142850',
    backgroundColor:'white',
    fontSize: 15,
  },

});
export default RestaurantForm
