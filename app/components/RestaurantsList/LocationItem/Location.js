import MapView,
{
  PROVIDER_GOOGLE,
  Marker,
  Callout
} from 'react-native-maps'

import React, { useState } from 'react'
import {
  View,
  Text,
  StyleSheet,
} from 'react-native'

export default RestaurantMap = ({ route }) => {
  const { name, location } = route.params
  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: -16.409046,
          longitude: -71.537453,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}>
        <Marker
          coordinate={{
            latitude: location.lat,
            longitude: location.lon,
          }}>
          <Callout>
            <View
              style={{
                backgroundColor: '#FFF',
                borderRadius: 5
              }}>
              <Text>
                {name}
              </Text>
            </View>
          </Callout>
        </Marker>
      </MapView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  map: {
    flex: 1
  }
})