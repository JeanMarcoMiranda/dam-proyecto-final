import React from 'react'
import {
  Image,
  View,
  Text, 
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native'
import firebase from '../../../../firebase'
import 'firebase/firestore'


const RestaurantCardItem = ({ item, navigation, changeState}) => {
  const {name, shortDescription, urlImage} = item

  const onPressDelete = () => {
    Alert.alert(
      "Advertencia",
      "¿Seguro que quieres eliminar este restaurante?",
      [
        {
          text: 'No',
          onPress: () => console.log('Cancelado'),
          style: 'cancel'
        },
        {
          text: 'Si',
          onPress: confirmDelete
        }
      ]
    )
  }

  const confirmDelete = async () => {
    console.log('Aceptado')
    try {
      const db = firebase.firestore()
      db.settings({ experimentalForceLongPolling: true })

      const restaurantRef = db.collection('restaurants')
      await restaurantRef.doc(item.id).delete()

      const restaurants = []
      const data = await restaurantRef.get()
      data.docs.forEach( doc => {
        const nuevoObjeto = {
          ...doc.data(),
          id : doc.id
        }
        restaurants.push(nuevoObjeto)
      })

      changeState(restaurants)
    } catch(error) {
      console.log(error)
      console.warn("Ha ocurrido un error al guardar los datos")
    }
  }

  return (
    <View style={styles.itemContainer}>
      <View style={styles.restaurantImageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: urlImage,
          }}
        />
      </View>
      <View style={styles.restaurantDescriptionContainer}>
        <Text style={styles.descriptionTitle}>
          {name}
        </Text>
        <Text style={styles.descriptionContent}>
          {shortDescription}
        </Text>
        <View style={styles.actionButtonItemContainer}>
          <TouchableOpacity style={[styles.actionButtonItem, {backgroundColor: '#00909E'}]}
            onPress={() => navigation.navigate('Form', {item, action : "Actualizar datos"})}>
            <Text style={styles.actionButtonItemText}>
              Editar
            </Text>
          </TouchableOpacity>

          <TouchableOpacity style={[styles.actionButtonItem, {backgroundColor: '#ff1600'}]}>
            <Text style={styles.actionButtonItemText}
              onPress={onPressDelete}>
              Eliminar
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    marginVertical: 10,
    borderRadius: 15,
    overflow: 'hidden',
    backgroundColor: '#27496D'
  },

  restaurantImageContainer: {
    height: 200,
    width: '100%'
  },

  image:{
    height: '100%',
    width: '100%'
  },

  restaurantDescriptionContainer: {
    padding: 15,
    borderBottomLeftRadius : 15,
    borderBottomRightRadius: 15,
    borderWidth: 1,
    borderColor: '#142850'
  },

  descriptionTitle:{
    fontWeight: 'bold',
    fontStyle: 'italic',
    fontSize: 20,
    color: 'white'
  },

  descriptionContent:{
    marginTop: 10,
    textAlign: 'justify',
    marginBottom: 15,
    color: 'white'
  },

  actionButtonItemContainer: {
    height: 40, 
    fontWeight: 'bold',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  actionButtonItem:{
    height: '100%',
    width:'47%',
    justifyContent: 'center',
    borderRadius: 10
  },

  actionButtonItemText: {
    textAlign: 'center',
    color: '#FFF',
    fontWeight: 'bold',
  }
})

export default RestaurantCardItem