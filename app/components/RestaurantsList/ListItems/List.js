import React, { useState, useEffect } from 'react'
import {
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native'
import RestaurantData from '../../../../RestaurantsData'
import RestaurantCardItem from './ListItem'
import firebase from '../../../../firebase'
import 'firebase/firestore'
import { Button } from 'react-native-paper'

const RestaurantsList = ({ navigation, route }) => {
  const changeView = (item) => {
    navigation.navigate('Details', item)
  }

  const [isLoading, setIsLoading] = useState(false)
  const [restaurantData, setRestaurantData] = useState([])

  const getData = async () => {
    const db = firebase.firestore()
    db.settings({ experimentalForceLongPolling: true })

    setIsLoading(true)
    const data = await db.collection('restaurants').get()

    const nuevaData = []
    data.docs.forEach(doc => {
      const nuevoObjeto = {
        ...doc.data(),
        id: doc.id
      }
      nuevaData.push(nuevoObjeto)
    })

    setRestaurantData(nuevaData)
    setIsLoading(false)
  }

  useEffect(() => {
    getData()
    return () => {
      setRestaurantData([])
    }
  }, [])

  return (
    <View style={styles.listContainer}>
      <FlatList
        refreshing={isLoading}
        onRefresh={getData}
        data={restaurantData}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => changeView(item)}
          >
            <RestaurantCardItem item={item} navigation={navigation} changeState={setRestaurantData} />
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
        showsVerticalScrollIndicator={false}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  listContainer: {
    width: '100%',
    height: '100%',
    paddingHorizontal: 20,
    paddingTop: 20,
    backgroundColor: '#DDDDDD'
  },

  buttonContainer: {
    height: 80
  },

  button: {
    marginTop: 15,
    height: 50,
    padding: 15,
    width: '100%',
    backgroundColor: '#4BF075',
    borderRadius: 10,
  }
})

export default RestaurantsList