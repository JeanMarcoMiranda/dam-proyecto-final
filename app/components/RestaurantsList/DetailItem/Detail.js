import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity
} from 'react-native'


const RestaurantDetail = ({ route, navigation }) => {
  const { location, name, longDescription, urlImage } = route.params
  return (
    <ScrollView style={styles.scrollViewContainer} showsVerticalScrollIndicator={false}>
      <View style={styles.restaurantImageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: urlImage,
          }}
        />
      </View>
      <View style={styles.restaurantDescriptionContainer}>
        <Text style={styles.descriptionTitle}>
          {name}
        </Text>
        <Text style={styles.descriptionContent}>
          {longDescription}
        </Text>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Maps', { name, location })}
        >
          <Text style={{textAlign: 'center', fontSize: 15, color: 'white'}}>
            Ver ubicación
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({

  scrollViewContainer: {
    paddingHorizontal: 15,
    backgroundColor: '#DDDDDD'
  },

  restaurantImageContainer: {
    height: 250,
    marginTop: 20,
    width: "100%",
  },

  image: {
    height: '100%',
    width: '100%',
    borderRadius: 10
  },

  restaurantDescriptionContainer: {
    marginTop: 15
  },

  descriptionTitle: {
    fontWeight: 'bold',
    fontStyle: 'italic',
    fontSize: 20,
    textAlign: 'center'
  },

  descriptionContent: {
    marginTop: 10,
    paddingHorizontal: 5,
    textAlign: 'justify'
  },

  buttonContainer: {
    height: 80
  },

  button: {
    marginTop: 15,
    height: 50,
    padding: 15,
    width: '100%',
    backgroundColor: '#00AEEF',
    borderRadius: 10,
  }
})

export default RestaurantDetail