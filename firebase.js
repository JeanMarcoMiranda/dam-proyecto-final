import firebase from 'firebase'


const firebaseConfig = {
  apiKey: "AIzaSyAgp7AgofHt0yF29DRPuuXOWyZonHFyI5k",
  authDomain: "tecsup-projects.firebaseapp.com",
  databaseURL: "https://tecsup-projects.firebaseio.com",
  projectId: "tecsup-projects",
  storageBucket: "tecsup-projects.appspot.com",
  messagingSenderId: "255469457764",
  appId: "1:255469457764:web:d5b2851c299c9c03e8ea71"
};

firebase.initializeApp(firebaseConfig)


export default firebase

